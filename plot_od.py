import argparse
import pandas as pd
import numpy as np
import plotly.graph_objects as go

from plt_utils import plot_with_error, plot_line, plot_raw_line

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Plot the state estimate and covariance')
    parser.add_argument('csv', type=str, help='CSV file with estimates')
    parser.add_argument("--html", type=str, help="Save the HTML to a file")
    parser.add_argument(
        "--truth",
        type=str,
        required=False,
        help=
        "If provided, the difference between the truth trajectory and the estimated trajectory will be plotted"
    )
    parser.add_argument("--title", type=str, required=True, help="Plot title")
    parser.add_argument("--covar",
                        action='store_true',
                        default=False,
                        help="Plot the covariance only plot")
    parser.add_argument("--dev",
                        action='store_true',
                        default=False,
                        help="Plot the state deviation plot")
    parser.add_argument("--est",
                        action='store_true',
                        default=False,
                        help="Plot the state estimate plot")
    parser.add_argument("--copy",
                        type=str,
                        default=None,
                        help="Add copyright watermark")
    args = parser.parse_args()

    if args.html and '.html' in args.html:
        html_out = args.html.replace('.html', '_{}.html')
    else:
        html_out = None

    df = pd.read_csv(args.csv)

    try:
        time_col = df['Epoch:GregorianUtc']
    except KeyError:
        time_col = df['Epoch:GregorianTai']

    # Reference the covariance frames
    for covar_var in [
            'cx_x', 'cy_y', 'cz_z', 'cx_dot_x_dot', 'cy_dot_y_dot',
            'cz_dot_z_dot'
    ]:
        possible_frames = ["", ":ric", ":rcn", ":vnc"]
        for frame in possible_frames:
            if f'{covar_var}{frame}' in df:
                locals()[covar_var] = f'{covar_var}{frame}'

        if covar_var not in locals():
            raise KeyError(f"Cannot find {covar_var} covariance column")

    plotted_something = False

    # Build the state estimate and covariance plot
    if args.est:
        plotted_something = True
        fig = go.Figure()
        plot_with_error(fig, df, time_col, 'Estimate:X (km)', cx_x, 'blue',
                        'Position estimate (km)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'Estimate:Y (km)', cy_y, 'green',
                        'Position estimate (km)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'Estimate:Z (km)', cz_z, 'orange',
                        'Position estimate (km)', args.title, args.copy)
        fig.show()
        if html_out:
            this_output = html_out.format("pos_est")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

        fig = go.Figure()
        plot_with_error(fig, df, time_col, 'Estimate:VX (km/s)', cx_dot_x_dot,
                        'blue', 'Velocity estimate (km)', args.title,
                        args.copy)
        plot_with_error(fig, df, time_col, 'Estimate:VY (km/s)', cy_dot_y_dot,
                        'green', 'Velocity estimate (km)', args.title,
                        args.copy)
        plot_with_error(fig, df, time_col, 'Estimate:VZ (km/s)', cz_dot_z_dot,
                        'orange', 'Velocity estimate (km)', args.title,
                        args.copy)
        fig.show()
        if html_out:
            this_output = html_out.format("vel_est")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

    # Build the state deviation and covariance plot (useful to assess filter convergence)
    if args.dev:
        plotted_something = True
        fig = go.Figure()
        plot_with_error(fig, df, time_col, 'delta_x', cx_x, 'blue',
                        'Position deviation (km)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'delta_y', cy_y, 'green',
                        'Position deviation (km)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'delta_z', cz_z, 'orange',
                        'Position deviation (km)', args.title, args.copy)
        # Autoscale
        hwpt = int(len(df) / 2)
        max_cov_y = max(max(df[cx_x][hwpt:]), max(df[cy_y][hwpt:]),
                        max(df[cz_z][hwpt:]))
        fig.update_layout(yaxis_range=[-1.5 * max_cov_y, 1.5 * max_cov_y])
        fig.show()
        if html_out:
            this_output = html_out.format("pos_dev")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

        fig = go.Figure()
        plot_with_error(fig, df, time_col, 'delta_vx', cx_dot_x_dot, 'blue',
                        'Velocity deviation (km/s)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'delta_vy', cy_dot_y_dot, 'green',
                        'Velocity deviation (km/s)', args.title, args.copy)
        plot_with_error(fig, df, time_col, 'delta_vz', cz_dot_z_dot, 'orange',
                        'Velocity deviation (km/s)', args.title, args.copy)
        # Autoscale
        hwpt = int(len(df) / 2)
        max_cov_y = max(max(df[cx_dot_x_dot][hwpt:]),
                        max(df[cy_dot_y_dot][hwpt:]),
                        max(df[cz_dot_z_dot][hwpt:]))

        fig.update_layout(yaxis_range=[-1.5 * max_cov_y, 1.5 * max_cov_y])
        fig.show()
        if html_out:
            this_output = html_out.format("vel_dev")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

    # Build the covariance only plot
    if args.covar:
        plotted_something = True
        fig = go.Figure()
        plot_line(fig, df, time_col, cx_x, 'blue', 'Position covariance (km)',
                  args.title, args.copy)
        plot_line(fig, df, time_col, cy_y, 'green', 'Position covariance (km)',
                  args.title, args.copy)
        plot_line(fig, df, time_col, cz_z, 'orange',
                  'Position covariance (km)', args.title, args.copy)
        # Autoscale
        hwpt = int(len(df) / 2)
        max_cov_y = max(max(df[cx_x][hwpt:]), max(df[cy_y][hwpt:]),
                        max(df[cz_z][hwpt:]))
        fig.update_layout(yaxis_range=[-0.1 * max_cov_y, 1.5 * max_cov_y])
        fig.show()
        if html_out:
            this_output = html_out.format("pos_cov")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

        fig = go.Figure()
        plot_line(fig, df, time_col, cx_dot_x_dot, 'blue',
                  'Velocity covariance (km/s)', args.title, args.copy)
        plot_line(fig, df, time_col, cy_dot_y_dot, 'green',
                  'Velocity covariance (km/s)', args.title, args.copy)
        plot_line(fig, df, time_col, cz_dot_z_dot, 'orange',
                  'Velocity covariance (km/s)', args.title, args.copy)
        # Autoscale
        hwpt = int(len(df) / 2)
        max_cov_y = max(max(df[cx_dot_x_dot][hwpt:]),
                        max(df[cy_dot_y_dot][hwpt:]),
                        max(df[cz_dot_z_dot][hwpt:]))

        fig.update_layout(yaxis_range=[-0.1 * max_cov_y, 1.5 * max_cov_y])
        fig.show()
        if html_out:
            this_output = html_out.format("vel_cov")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

    if args.truth:
        plotted_something = True
        truth = pd.read_csv(args.truth).drop_duplicates('Epoch:GregorianUtc')

        # Ensure perfect overlap of the data
        # Find the estimates where there is truth data
        cmp_df = df.loc[df['Epoch:GregorianUtc'].isin(
            truth['Epoch:GregorianUtc'])]

        # Find the truth data where there is estimate data
        truth_df = truth.loc[truth['Epoch:GregorianUtc'].isin(
            cmp_df['Epoch:GregorianUtc'])]

        err_x = np.array(cmp_df["Estimate:X (km)"]) - truth_df["X (km)"]
        err_y = np.array(cmp_df["Estimate:Y (km)"]) - truth_df["Y (km)"]
        err_z = np.array(cmp_df["Estimate:Z (km)"]) - truth_df["Z (km)"]

        err_vx = np.array(cmp_df["Estimate:VX (km/s)"]) - truth_df["VX (km/s)"]
        err_vy = np.array(cmp_df["Estimate:VY (km/s)"]) - truth_df["VY (km/s)"]
        err_vz = np.array(cmp_df["Estimate:VZ (km/s)"]) - truth_df["VZ (km/s)"]

        time_col = cmp_df['Epoch:GregorianUtc']

        # Plot the position errors
        fig = go.Figure()
        plot_raw_line(fig, time_col, err_x, "X error (km)", 'blue',
                      "Estimate - Truth (km)", args.title, args.copy)
        plot_raw_line(fig, time_col, err_y, "Y error (km)", 'green',
                      "Estimate - Truth (km)", args.title, args.copy)
        plot_raw_line(fig, time_col, err_z, "Z error (km)", 'yellow',
                      "Estimate - Truth (km)", args.title, args.copy)

        # Autoscale
        hwpt = int(len(cmp_df) / 2)
        max_y = max(max(err_x[hwpt:]), max(err_y[hwpt:]), max(err_z[hwpt:]))

        fig.update_layout(yaxis_range=[-1.5 * max_y, 1.5 * max_y])
        fig.show()
        if html_out:
            this_output = html_out.format("pos_truth_error")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

        # Plot the velocity errors
        fig = go.Figure()
        plot_raw_line(fig, time_col, err_vx, "VX error (km/s)", 'blue',
                      "Estimate - Truth (km/s)", args.title, args.copy)
        plot_raw_line(fig, time_col, err_vy, "VY error (km/s)", 'green',
                      "Estimate - Truth (km/s)", args.title, args.copy)
        plot_raw_line(fig, time_col, err_vz, "VZ error (km)", 'yellow',
                      "Estimate - Truth (km/s)", args.title, args.copy)

        # Autoscale
        hwpt = int(len(cmp_df) / 2)
        max_y = max(max(err_vx[hwpt:]), max(err_vy[hwpt:]), max(err_vz[hwpt:]))

        fig.update_layout(yaxis_range=[-1.5 * max_y, 1.5 * max_y])
        fig.show()
        if html_out:
            this_output = html_out.format("vel_truth_error")
            with open(this_output, 'w') as f:
                f.write(fig.to_html())
            print(f"Saved HTML to {this_output}")

    if not plotted_something:
        raise ValueError("You didn't opt to plot anything dummy!")