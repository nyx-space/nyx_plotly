# Nyx Plotly
A set of plotly utilities for different analyses.

+ Mission design in the `md` folder
+ Orbit determination in the `od` folder

## Getting started

1. Install the dependencies with `pipenv install`
2. Run the desired script as `pipenv run python THE_SCRIPT.py --help`