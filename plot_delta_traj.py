import argparse
import pandas as pd
import plotly.graph_objects as go
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=
        'Plot the difference between the first trajectory CSV and all the other ones'
    )
    parser.add_argument('csv',
                        metavar='N',
                        type=str,
                        nargs='+',
                        help='list of CSV files')
    args = parser.parse_args()

    # Load all of the CSV files in data frames
    first_file = None
    fname = ""
    pos_err = []
    vel_err = []
    for fpath in args.csv:
        df = pd.read_csv(fpath)
        if first_file is None:
            first_file = df
            fname = fpath.split('/')[-1].replace('.csv', '')
            continue

        if df.shape[0] != first_file.shape[0]:
            print(f"File {fpath} is not the same length as the first file")
            continue

        pos_err += [
            np.sqrt((first_file['X (km)'] - df['X (km)'])**2 +
                    (first_file['Y (km)'] - df['Y (km)'])**2 +
                    (first_file['Z (km)'] - df['Z (km)'])**2)
        ]
        vel_err += [
            np.sqrt((first_file['VX (km/s)'] - df['VX (km/s)'])**2 +
                    (first_file['VY (km/s)'] - df['VY (km/s)'])**2 +
                    (first_file['VZ (km/s)'] - df['VZ (km/s)'])**2)
        ]

    # Now that we have the data, let's plot it

    # Initialize the errors plot
    fig = go.Figure()
    for (no, err) in enumerate(pos_err):
        min_e = np.min(err)
        max_e = np.max(err)
        avr_e = np.sum(err) / err.shape[0]
        fig.add_trace(
            go.Scatter(
                x=first_file['Epoch:GregorianUtc'],
                y=err,
                mode='lines',
                name=
                f'Pos. wrt #{no+1}: max: {max_e:.1f} km   avr: {avr_e:.1f} km   min: {min_e:.1f} km  '
            ))

    for (no, err) in enumerate(vel_err):
        min_e = np.min(err)
        max_e = np.max(err)
        avr_e = np.sum(err) / err.shape[0]
        fig.add_trace(
            go.Scatter(
                x=first_file['Epoch:GregorianUtc'],
                y=err,
                mode='lines',
                name=
                f'Vel. wrt #{no+1}: max: {max_e:.1f} km/s avr: {avr_e:.1f} km/s min: {min_e:.1f} km/s'
            ))
    fig.update_layout(showlegend=True, title=fname)
    fig.show()
