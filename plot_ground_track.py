import argparse
import pandas as pd
import plotly.graph_objects as go

from plt_utils import (
    colors,
    finalize_plot,
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Plot a ground track (or set thereof)')
    parser.add_argument('csv',
                        metavar='N',
                        type=str,
                        nargs='+',
                        help='list of CSV files')
    parser.add_argument("--html", type=str, help="Save the HTML to a file")
    parser.add_argument("--not-earth",
                        action='store_true',
                        help="Only show the grid, not the Earth land borders")
    parser.add_argument(
        "-p",
        "--projection",
        default="orthographic",
        help=
        'Select projection from one of equirectangular, mercator, orthographic, '
        +
        'natural earth, kavrayskiy7, miller, robinson, eckert4, azimuthal equal area, '
        +
        'azimuthal equidistant, conic equal area, conic conformal, conic equidistant, '
        +
        'gnomonic, stereographic, mollweide, hammer, transverse mercator, albers usa, '
        + 'winkel tripel, aitoff, sinusoidal')
    parser.add_argument(
        "-l",
        "--landmarks",
        type=str,
        required=False,
        help=
        "A list of landmarks, format example: `Eiffel tower: (2.2945, 48.8584)`"
    )
    args = parser.parse_args()

    # Initialize the plot
    fig = go.Figure()

    # Load all of the CSV files in data frames
    color_values = list(colors.values())
    for (i, fpath) in enumerate(args.csv):
        df = pd.read_csv(fpath)
        name = fpath.split('/')[-1].replace('.csv', '')
        try:
            color = color_values[i % len(color_values)]
            fig.add_trace(
                go.Scattergeo(
                    lon=df['GeodeticLongitude (deg)'],
                    lat=df['GeodeticLatitude (deg)'],
                    mode='lines',
                    name=name,
                    line=dict(
                        color=
                        f"rgb({int(color[0])}, {int(color[1])}, {int(color[2])})",
                    ),
                ))
        except KeyError as e:
            print(
                "\n\nLatitude and longitude not found in the data.\n"
                "Generate the output with traj.to_csv_with_groundtrack(...), \n"
                "Or specify `\"geodetic_latitude\", \"geodetic_longitude\"` in the output headers\n\n"
            )
            raise e

    if args.landmarks:
        for l in args.landmarks.split(';'):
            name, pos = l.strip().split(':')
            long, lat = pos.replace(')', '').replace('(',
                                                     '').strip().split(',')
            fig.add_trace(
                go.Scattergeo(lon=[float(long)],
                              lat=[float(lat)],
                              name=name.strip()))

    fig.update_geos(lataxis_showgrid=True,
                    lonaxis_showgrid=True,
                    projection_type=args.projection,
                    visible=not args.not_earth)
    fig.update_layout(height=300, margin={"r": 0, "t": 0, "l": 0, "b": 0})
    fig.show()
    if args.html:
        with open(args.html, 'w') as f:
            f.write(fig.to_html())
        print(f"Saved HTML to {args.html}")
